package uzb.farhod.lesson6_cascade.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson6_cascade.entity.Person;

public interface PersonRepository extends JpaRepository<Person , Integer> {
}
