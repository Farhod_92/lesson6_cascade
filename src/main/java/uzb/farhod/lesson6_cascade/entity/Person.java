package uzb.farhod.lesson6_cascade.entity;

import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Period;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
//@Where(clause = "birth_date is not null")
//@Where(clause = "birth_date='2021-08-06'")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String fullName;

    @OrderBy(value = "city asc , street desc")
    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<Address> addresses;


    private LocalDate birthDate;

    @Transient
    private Integer countNameLetters;

    @Transient
    private Integer age;

    public Integer getCountNameLetters() {
       return  fullName==null?0:fullName.length();
    }

    public Integer getAge(){
        if(birthDate==null)
            return 0;
        return Period.between(birthDate,LocalDate.now()).getYears();
    }
}
