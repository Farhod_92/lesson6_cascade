package uzb.farhod.lesson6_cascade;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lesson6CascadeApplication {

    public static void main(String[] args) {
        SpringApplication.run(Lesson6CascadeApplication.class, args);
    }

}
