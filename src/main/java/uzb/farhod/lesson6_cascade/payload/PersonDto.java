package uzb.farhod.lesson6_cascade.payload;

import lombok.Getter;

import java.util.List;

@Getter
public class PersonDto {
    private String fullName;

    private List<AddressDto> addressDtoList;
}
