package uzb.farhod.lesson6_cascade.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson6_cascade.entity.Address;
import uzb.farhod.lesson6_cascade.entity.Person;
import uzb.farhod.lesson6_cascade.payload.AddressDto;
import uzb.farhod.lesson6_cascade.payload.PersonDto;
import uzb.farhod.lesson6_cascade.repository.ClientRepository;
import uzb.farhod.lesson6_cascade.repository.PersonRepository;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/client")
public class ClientController {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    ClientRepository clientRepository;
    
    @DeleteMapping("/{id}")
    public HttpEntity<?> delete(@PathVariable Integer id){
        try{
           clientRepository.deleteById(id);
           return ResponseEntity.ok("deleted");
        }catch (Exception e){
           return ResponseEntity.ok("not deleted");
        }
    }
}
