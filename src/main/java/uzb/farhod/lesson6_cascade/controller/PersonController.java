package uzb.farhod.lesson6_cascade.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson6_cascade.entity.Address;
import uzb.farhod.lesson6_cascade.payload.AddressDto;
import uzb.farhod.lesson6_cascade.repository.AddressRepository;
import uzb.farhod.lesson6_cascade.repository.PersonRepository;
import uzb.farhod.lesson6_cascade.entity.Person;
import uzb.farhod.lesson6_cascade.payload.PersonDto;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/person")
public class PersonController {

    @Autowired
    PersonRepository personRepository;

//    @Autowired
//    AddressRepository addressRepository;

    @Transactional(noRollbackFor = NullPointerException.class)
    @PostMapping
    public HttpEntity<?> addPerson(@RequestBody PersonDto personDto){
        //PERSON YASAB OLDIK
        Person person=new Person();
        person.setFullName(personDto.getFullName());


        //ADDRESS YASAB OLAMIZ
        List<Address> addresses=new ArrayList<>();
        for (AddressDto addressDto : personDto.getAddressDtoList()) {
            Address address=new Address(
                    addressDto.getStreet(),
                    addressDto.getCity(),
                    person
            );
            addresses.add(address);
        }
       // addressRepository.saveAll(addresses);
        person.setAddresses(addresses);
        personRepository.save(person);
//        String var=null;
//        boolean test=var.equals("test");
        return ResponseEntity.ok("saved");
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editPerson(@PathVariable Integer id){
        //PERSON YASAB OLDIK
        Person person= personRepository.getById(id);
        person.setFullName("bbbb");


        //ADDRESS YASAB OLAMIZ
        List<Address> addresses=new ArrayList<>();
        for (Address address : person.getAddresses()) {
            address.setStreet("bbbbbb");
            addresses.add(address);
        }
        person.setAddresses(addresses);
        personRepository.save(person);

        return ResponseEntity.ok("saved");
    }
    
    @DeleteMapping("/{id}")
    public HttpEntity<?> delete(@PathVariable Integer id){
        try{
           personRepository.deleteById(id);
           return ResponseEntity.ok("deleted");
        }catch (Exception e){
           return ResponseEntity.ok("not deleted");
        }
    }

    @DeleteMapping("/transaction/{id}")
    public  HttpEntity<?> deleteTransactional(@PathVariable Integer id){
        personRepository.deleteById(id);
        throw new NullPointerException();
        //return ResponseEntity.ok("o'chirildi");
    }

    @GetMapping
    public HttpEntity<?> getALl(){
        List<Person> all = personRepository.findAll();
        return ResponseEntity.ok(all);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getALl(@PathVariable Integer id){
        return ResponseEntity.ok(personRepository.findById(id).get());
    }
}
