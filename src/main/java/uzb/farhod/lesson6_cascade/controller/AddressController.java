package uzb.farhod.lesson6_cascade.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson6_cascade.entity.Address;
import uzb.farhod.lesson6_cascade.payload.AddressDto;
import uzb.farhod.lesson6_cascade.repository.AddressRepository;
import uzb.farhod.lesson6_cascade.repository.PersonRepository;
import uzb.farhod.lesson6_cascade.entity.Person;
import uzb.farhod.lesson6_cascade.payload.PersonDto;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/address")
public class AddressController {

    private final AddressRepository addressRepository;

    private final PersonRepository personRepository;

    public AddressController(AddressRepository addressRepository, PersonRepository personRepository) {
        this.addressRepository = addressRepository;
        this.personRepository = personRepository;
    }

    @PostMapping
    public HttpEntity<?> addPerson(@RequestBody List<AddressDto> addressDtoList){
        List<Address> addresses=new ArrayList<>();

        for (AddressDto addressDto : addressDtoList) {
            Address address=new Address(
                    addressDto.getStreet(),
                    addressDto.getCity(),
                    personRepository.getById(addressDto.getPersonId())
            );
        addresses.add(address);
        }

        addressRepository.saveAll(addresses);

        return ResponseEntity.ok("saved");
    }

    @GetMapping
    public HttpEntity<?> getALl(){
        List<Address> all = addressRepository.findAll();
        return ResponseEntity.ok(all);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getOne(@PathVariable Integer id){
        Address byId = addressRepository.getById(id);
        return ResponseEntity.ok(byId);
    }
}
